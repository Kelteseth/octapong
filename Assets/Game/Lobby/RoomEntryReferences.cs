﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RoomEntryReferences : MonoBehaviour 
{
    public Text RoomName;

    public void JoinRoom()
    {
        PhotonNetwork.JoinRoom(RoomName.text);
    }
}
