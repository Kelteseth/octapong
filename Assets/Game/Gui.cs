﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class Gui : Photon.PunBehaviour {

    public GameObject ConnectingToPhoton;
    public GameObject JoinLobby;
    public GameObject Main; 
    public GameObject JoinRoom;
    public GameObject Room;

    public GameObject GameListRoot;
    public GameObject RoomEntryPrefab;

    public List<RoomInfo> Rooms = new List<RoomInfo>();

    public Text ChatWindow;
    public InputField ChatBox;

    public List<RoomInfo> GameList = new List<RoomInfo>();

	// Use this for initialization
	void Start () 
    {
        PhotonNetwork.ConnectUsingSettings("1.0");
        ConnectingToPhoton.SetActive(true);
	}

    public override void OnConnectedToPhoton()
    {
        ConnectingToPhoton.SetActive(false);
        Debug.Log("Connected to Photon");
        JoinLobby.SetActive(true);
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
{
 	    JoinLobby.SetActive(false);
        Main.SetActive(true);
    }
	
	public void ChangePlayerName(string newPlayerName)
    {
        PhotonNetwork.player.name = newPlayerName;
    }

    public void CreateNewRoom()
    {
        PhotonNetwork.CreateRoom(PhotonNetwork.player.name + "s Game");
        Main.SetActive(false);
        JoinRoom.SetActive(true);
    }

    public override void OnJoinedRoom()
    {
        JoinRoom.SetActive(false);
        Main.SetActive(false);
        Room.SetActive(true);
    }

    public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
    {
        Main.SetActive(true);
        JoinRoom.SetActive(false);
    }

    public override void OnReceivedRoomListUpdate()
    {
        for(int i = GameListRoot.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(GameListRoot.transform.GetChild(i));
        }
        Rooms = PhotonNetwork.GetRoomList().ToList();

        foreach(RoomInfo room in Rooms)
        {
            GameObject go = Instantiate(RoomEntryPrefab);
            go.transform.SetParent(GameListRoot.transform);
            go.name = room.name;
            go.GetComponent<RoomEntryReferences>().RoomName.text = room.name;
        }
    }

    public void SendChatMessage(string text)
    {
        text = PhotonNetwork.player.name + ": " + text + "\n";
        EnterChatMessage(text);
        photonView.RPC("EnterChatMessage", PhotonTargets.Others, text);
        ChatBox.text = "";
    }

    [PunRPC]
    public void EnterChatMessage(string text)
    {
        ChatWindow.text += text;
    }

    public void StartGame()
    {
        PhotonNetwork.LoadLevel("Game");
    }
}
