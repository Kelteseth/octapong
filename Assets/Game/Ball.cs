﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class Ball : Photon.PunBehaviour
{
	public float _speed = 1.0f;
	Rigidbody _rigidBody = null;

	void Awake()
	{
		_rigidBody = GetComponent<Rigidbody>();
	}
	void Start()
	{
		_rigidBody.velocity = Random.insideUnitSphere;
	}
	void FixedUpdate()
	{
		_rigidBody.velocity = _rigidBody.velocity.normalized * _speed;
	}
	void OnTriggerExit(Collider c)
	{
		if (c.gameObject.tag != "Deathzone")
		{
			return;
		}

		c.gameObject.transform.parent.gameObject.GetComponent<Player>().ReduceLife();

		StartCoroutine(ResetBall());
	}

	IEnumerator ResetBall()
	{
		yield return new WaitForSeconds(2);

		transform.position = Vector3.zero;
	}

    
}
