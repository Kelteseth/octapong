﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    public int lifes = 5;
    public Vector3 _direction = new Vector3(1, 0, 0);
    public bool _isHorizontal = true;
	public bool _isAI = false;
	public bool _isAlive = true;

    Rigidbody _rigid = null;
	bool _aiMoveForward = true;

    void Awake()
    {
        _rigid = GetComponent<Rigidbody>();

        if (_isHorizontal)
        {
            _rigid.constraints = RigidbodyConstraints.FreezeAll & ~RigidbodyConstraints.FreezePositionX;
			_direction = new Vector3(1, 0, 0);
        }
        else
        {
            _rigid.constraints = RigidbodyConstraints.FreezeAll & ~RigidbodyConstraints.FreezePositionZ;
			_direction = new Vector3(0, 0, 1);
        }
    }

    void FixedUpdate()
    {
		if (!_isAlive)
		{
			return;
		}

		if (_isAI)
		{
			if (_isHorizontal)
			{
				if (_aiMoveForward)
				{
					_rigid.AddForce(_direction, ForceMode.VelocityChange);
				}
				else
				{
					_rigid.AddForce(-_direction, ForceMode.VelocityChange);
				}
			}
			else
			{
				if (_aiMoveForward)
				{
					_rigid.AddForce(_direction, ForceMode.VelocityChange);
				}
				else
				{
					_rigid.AddForce(-_direction, ForceMode.VelocityChange);
				}
			}
		}
		else
		{
			if (_isHorizontal)
			{
				_rigid.AddForce(_direction * Input.GetAxis("Horizontal"), ForceMode.VelocityChange);
			}
			else
			{
				_rigid.AddForce(_direction * Input.GetAxis("Vertical"), ForceMode.VelocityChange);
			}
		}
    }

    public void ReduceLife()
    {
        if (--lifes <= 0)
        {
            print(transform.name + " is dead!");
			_isAlive = false;
            Time.timeScale = 0;
        }
    }

	void OnCollisionEnter(Collision c)
	{
		if (!_isAI || c.gameObject.tag != "Wall")
		{
			return;
		}

		_aiMoveForward = !_aiMoveForward;
	}
}
